#!/usr/bin/env python

import os
import sys

if __name__ == "__main__":
    settings_file = 'local' if os.environ.get('IS_LOCAL') else 'production'
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "demo.settings." + settings_file)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
