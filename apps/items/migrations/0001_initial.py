# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-30 11:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('serial_number', models.CharField(max_length=5)),
                ('description', models.CharField(max_length=100)),
                ('active', models.BooleanField(default=False)),
                ('date', models.DateField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
