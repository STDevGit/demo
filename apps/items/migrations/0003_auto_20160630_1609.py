# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-30 16:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0002_auto_20160630_1534'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='created',
        ),
        migrations.RemoveField(
            model_name='item',
            name='modified',
        ),
    ]
