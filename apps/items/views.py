# -*- coding: utf-8 -*-

from datetime import datetime

from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import UpdateAPIView, ListAPIView
from rest_framework import status

from .models import Item
from .serializers import ItemSerializer, ItemsImportSerializer, ItemUpdateSerializer


class ItemsImportAPIView(APIView):

    serializer_class = ItemsImportSerializer

    def post(self, request):
        """
        ---
        request_serializer: ItemsImportSerializer
        POST:
        parameters:
            - name: file
              description: csv file
              required: true
              type: file
        """

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.create(request.data)
            return Response('successfully imported', status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ItemUpdateAPIView(UpdateAPIView):

    serializer_class = ItemUpdateSerializer

    def get_object(self):
        return get_object_or_404(Item, serial_number=self.kwargs['serial_number'])


class ItemRetrieveAPIView(ListAPIView):

    def list(self, request, date):

        try:
            datetime.strptime(self.kwargs['date'], '%Y-%m-%d')
        except ValueError:
            return Response('Date field must be in year-month-day format', status=status.HTTP_400_BAD_REQUEST)

        return super().list(request)

    serializer_class = ItemSerializer

    def get_queryset(self):
        return Item.objects.filter(date=self.kwargs['date'])

