# -*- coding: utf-8 -*-

import csv
from datetime import datetime

from rest_framework import serializers

from .models import Item


class ItemsImportSerializer(serializers.Serializer):

    file = serializers.FileField(required=True)

    def create(self, validated_data):

        items_data = self.read_from_csv(validated_data)
        Item.objects.bulk_create(items_data)

        return items_data

    def validate(self, data):

        if not data['file'].name.lower().endswith('.csv'):
            raise serializers.ValidationError({'file': 'File must be in .csv format'})

        return data

    def read_from_csv(self, validated_data):

        items_data = list()
        serial_numbers_list = list()

        with open(validated_data['file'].temporary_file_path(), 'rt') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:

                # check each raw/column validation
                if not self._is_valid_active(row[2]):
                    raise serializers.ValidationError('Active field must be only 1 or 0')
                if not self._is_valid_date(row[3]):
                    raise serializers.ValidationError('Date field must be in year-month-day format')

                serial_numbers_list.append(row[0])

                items_data.append(
                    Item(
                        serial_number=row[0],
                        description=row[1],
                        active=row[2],
                        date=row[3],
                    )
                )

        if not self.is_unique_data(serial_numbers_list):
            raise serializers.ValidationError('Serial numbers must be unique')

        return items_data

    def _is_valid_active(self, value):

        allowed_values = ('0', 0, '1', 1)
        return True if value in allowed_values else False

    def _is_valid_date(self, value):

        try:
            datetime.strptime(value, '%Y-%m-%d')
            return True
        except ValueError:
            return False

    def is_unique_data(self, data):

        if len(data) > len(set(data)):
            return False
        return False if Item.objects.filter(serial_number__in=data).exists() else True


class ItemUpdateSerializer(serializers.Serializer):

    active = serializers.BooleanField()

    def update(self, instance, validated_data):

        instance.active = validated_data.get('active', False)
        instance.save()

        return instance


class ItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
