# -*- coding: utf-8 -*-

from django.db import models


class Item(models.Model):

    serial_number = models.CharField(max_length=5, unique=True)
    description = models.CharField(max_length=100)
    active = models.BooleanField(default=False)
    date = models.DateField()

    def __unicode__(self):
        return self.serial_number

    def __str__(self):
        return self.serial_number

    class Meta:
        ordering = ['-id']
