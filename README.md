=================
Demo
=================


-----------------
Development setup
-----------------

Install required system packages:

.. code-block:: bash

    $ sudo apt-get install python3-pip
    $ sudo apt-get install libpq-dev
    $ sudo apt-get install postgresql
    $ sudo apt-get install postgresql-contrib
	$ sudo apt-get install python-dev
    
Create www directory where project sites and environment dir

.. code-block:: bash

    $ mkdir /var/www && mkdir /var/envs && mkdir /var/envs/bin
    
Install virtualenvwrapper

.. code-block:: bash

    $ sudo pip3 install virtualenvwrapper
    $ sudo pip3 install --upgrade virtualenv==15.0.1
    
    
Add these to your bashrc virutualenvwrapper work

.. code-block:: bash

    export WORKON_HOME=/var/envs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export PROJECT_HOME=/var/www
    export VIRTUALENVWRAPPER_HOOK_DIR=/var/envs/bin
    source /usr/local/bin/virtualenvwrapper.sh
    
Create virtualenv

.. code-block:: bash

    $ cd /var/envs && mkvirtualenv --python=/usr/bin/python3 demo
    
Install requirements for a project.

.. code-block:: bash

    $ cd /var/www/demo && pip install -r requirements/local.txt

Configure DB

.. code-block:: bash

    $ sudo su - postgres
    $ createdb demo
    $ psql
    $ CREATE USER root;
    $ ALTER USER root PASSWORD 'root';
