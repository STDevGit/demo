# -*- coding: utf-8 -*-


from demo.settings.base import *


DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = True
THUMBNAIL_DEBUG = True

BASE_URL = 'http://demo.codebnb.me/'

ALLOWED_HOSTS = ['*', ]

MIDDLEWARE_CLASSES += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'demo',
        'USER': 'demo',
        'PASSWORD': '82AVbbR6JByqNqxb',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}


INSTALLED_APPS = DEFAULT_APPS + THIRD_PARTY_APPS + PROJECT_APPS

ADMINS = (
     ('Mushegh Eghiazaryan', 'mushegh@stdevmail.com'),
     ('Arman Kardashian', 'arman.kardashian@gmail.com'),
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        'django.security': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}

# Email configs.
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = '<username>'
EMAIL_HOST_PASSWORD = '<password>'
EMAIL_PORT = 587
