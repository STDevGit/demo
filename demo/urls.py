# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin

from items.views import ItemsImportAPIView, ItemUpdateAPIView, ItemRetrieveAPIView

urlpatterns = [

    # admin part
    url(r'^admin/', admin.site.urls),

    # client part
    # url(r'^', include(router.urls)),
    url(r'^', include('rest_framework_swagger.urls')),
    url(r'item-update/(?P<serial_number>\w+)', ItemUpdateAPIView.as_view(), name='item-update'),
    url(r'item-retrieve/(?P<date>.*)', ItemRetrieveAPIView.as_view(), name='item-update'),
    url(r'items-import/', ItemsImportAPIView.as_view(), name='items-import'),

]
